#!/bin/awk

BEGIN{
	instrument="M01371"
	run="1"
	flowcell="1"
	lane="1"
	tile="1"
	#x="1"
	#y="1"
	direction=readside
	filtered="N"
	flags="0"
	tag="8"

}
(NR % 4 == 1) {
	## set/randomize all these variables
	## example from pilot data "@M01371:115:000000000-B9BCT:1:1101:18660:2042:88"
	x=NR
	y=NR
	print "@" instrument ":" run ":" flowcell ":" lane ":" tile ":" x ":" y " " direction ":" filtered ":" flags ":" tag
	#instrument:run:flowcell:lane:tile:x:y direction:filtered:flags:tag.
} 
(NR%4!=1) {
	print $0
}
