#!/bin/awk
BEGIN{nbases=""}

## add stagger region to beginning of read
(NR % 4==2){
	## randomize how many bases to add
	nbases=randint(length(stagger_string)+1)
	## pull bases from end of stagger string
	toadd=substr(stagger_string, length(stagger_string)+1-nbases, nbases)
	print toadd $0
}

((NR % 4==1) || (NR%4==3)){
	print $0
}

## add quality scores at the beginning --> record how many bases were added
(NR % 4==0){	
	if(nbases=="") {
		print "Error: number of bases not initialized"
		exit
	} else {
		for(i=1; i<=nbases; i++) {
			printf "I" 
		}
		printf $0
		printf "\n"
	}
	nbases=""
}
## returns random integer less than n (not including n)
function randint(n){
	return int(n * rand())
}
