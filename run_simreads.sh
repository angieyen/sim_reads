#!/bin/bash
homedir="/cluster/home/yena/hb_crisprtx/ontarget/dataruns/sim_staggered"
AIDE_dir="/cluster/home/yena/hb_crisprtx/ontarget_bitbucket/amp_indel_detect"
amp_seq_coord_file="${homedir}/spy_info/staggered_spy2_amp_hg38.bed"
## run on simulated reads of different quality scores with mixed amplicons (final_datasets has mixture of wt, 1bp ins, 15bp del at different ratios) and unmixed amplicons (final_fastq)
for batchdir in ${homedir}/final_datasets/* 
#${homedir}/final_unstaggered_fastq/* ${homedir}/final_staggered_fastq/*;
do
    err_profile=`basename $batchdir`
    outdir="${homedir}/results/${err_profile}/"
	cmd="module load pandaseq; module load bwa; module load emboss; ${AIDE_dir}/run_batch.py --fastq_indir $batchdir --pipeline_outdir $outdir --amp_seq_coord_file $amp_seq_coord_file"
	qsub -q all.q -cwd -V -N "${err_profile}" -l h_vmem=16g -b y -o jobsoutput -e jobserror $cmd
	#echo $cmd
	#exit
done
