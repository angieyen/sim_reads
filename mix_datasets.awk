#!/bin/awk

BEGIN {
	srand(seed)
	## generate read numbers that we need to use
	for(i=1; i<=readstogenerate; i=i+1) {
		new_ind=int(rand()*readsincurrfile)
		while(new_ind in read_nums) {
			new_ind=int(rand()*readsincurrfile)
		}
		read_nums[new_ind]="yes"
	}
}

(int((NR-1)/4) in read_nums){
	print $0
}

