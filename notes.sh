#!/bin/bash

## Run amplicon sequencing simulation with ART: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3278762/
## Downloaded binary pack for 64-bit Linux system of ART-MountRainier-2016-06-05: https://www.niehs.nih.gov/research/resources/software/biostatistics/art/index.cfm
## amplicon sequencing simulation with paired-end reads
## input: 1) input sequence, 2) output file prefix, 3) read length, 4) read count
## default quality score profile based on read length
## -ss name of Illumina sequencing system of the built-in profile used for simulation (MSv1 is MiSeq v1 250bp)
## --qprof1 --qprof2 are first-read and second-read quality profiles if you learn them directly
## -amp amplicon sequencing simulation
## -p paired-end read simulation (generate reads from both ends of amplicons)
## -sam generates SAM alignment file
## -na do not output ALN alignment file
## -i input DNA/RNA reference
## -l length of simulated reads
## -f fold of read coverage to be simulated or number of reads/read pairs generated for each amplicon
## -o prefix of output filename
## default: art_illumina -ss GA2 -amp -p -sam -na -i amp_reference.fa -l 50 -f 10 -o amplicon_pair_dat

## Part 0: Generate custom error profiles in err_profiles/ with mk_all_err_profiles.sh

## Part 1: Simulate reads for different indels and error profiles
numreads=200000
readlength=300
if [[ 0 -eq 1 ]]; then
	mkdir -p "logs/"
	## Section B: Use custom error profiles where all reads are the same quality score (only affects substition error)
	for err_profile in "q10" "q20" "q30" "q40"
	do
		err_file="err_profiles/${err_profile}.txt"
		outdir="unstaggered_fastq/${err_profile}/"
		mkdir -p "${outdir}/"
		for edit in "wt" "1bpins" "15bpdel"
		do
			echo "Simulating $err_profile data for ${edit}..."
			outprefix="spy101_amp2_rev_${err_profile}_${edit}"
			
			art_illumina --qprof1 $err_file --qprof2 $err_file -amp -p -na -i ref_amplicons/spy101_amp2_hg38_rev_${edit}.fasta -l $readlength -f $numreads -o  ${outdir}/$outprefix > logs/${outprefix}.log
			for readside in 1 2; do
				outfile="${outdir}/${outprefix}${readside}.fq"
				gzip -f $outfile
			done
		done
	done
fi

# Part 2: make headers look realistic for pandaseq
if [[ 0 -eq 1 ]]; then
	echo "Fixing headers of simulated reads..."
	for readside in 1 2
	do
		for err_profile in "q10" "q20" "q30" "q40"
		do
			for edit in "wt" "1bpins" "15bpdel"
			do
				indir="unstaggered_fastq/${err_profile}"
				infile_prefix="spy101_amp2_rev_${err_profile}_${edit}"
				infile="${indir}/${infile_prefix}${readside}.fq.gz"
				
				outdir="final_unstaggered_fastq/${err_profile}"
				## "sample ID" must be first part of basename up to underscore and must be unique, so use edit and unstaggered to distinguish from other edits and staggered files
				## must fit *R1*.fastq.gz or *R2*.fastq.gz filename to find paired end files
				outbasefile="${edit}unstaggered_spy101_amp2_rev_R${readside}_${err_profile}.fastq"
				outfile="${outdir}/${outbasefile}"
				mkdir -p $outdir
				zcat "${infile}" | awk -vreadside=${readside} -f replace_fake_headers.awk >  $outfile
				gzip -f $outfile
			done
		done
	done
fi

## Part 3: add staggered ends of random length to reads
## forward: ATCGATCGA
## forward staggered + amp seq: ATCGATCGAGACTTGGGAGTTATCTGTAGTG
## reverse: AGCTAGCTA
## reverse staggered + amp seq: AGCTAGCTACATCACCAAGAGAGCCTTCC
if [[ 0 -eq 1 ]]; then
	echo "Adding staggered end of random length to front of reads..."
	for err_profile in "q10" "q20" "q30" "q40"
	do
		for edit in "wt" "1bpins" "15bpdel"
		do
			echo "Staggering $err_profile data for ${edit}..."
			for readside in 1 2
			do
				indir="final_unstaggered_fastq/${err_profile}"
				inbasefile="${edit}unstaggered_spy101_amp2_rev_R${readside}_${err_profile}.fastq.gz"
				infile="${indir}/${inbasefile}"
				
				outdir="final_staggered_fastq/${err_profile}"
				mkdir -p "${outdir}/"
				outbasefile="${edit}staggered_spy101_amp2_rev_R${readside}_${err_profile}.fastq"
				outfile="${outdir}/${outbasefile}"
				if [ $readside -eq 1 ]; then 
					stagger_string="ATCGATCGA"
				elif [ $readside -eq 2 ]; then 
					stagger_string="AGCTAGCTA"
				fi
				zcat ${infile} | awk -v stagger_string=${stagger_string} -f fake_staggered.awk > $outfile
				gzip -f $outfile
			done
		done
	done
fi


# Part 4: Mix reads from different indels to simulate real dataset
totalreads=100000
if [[ 0 -eq 1 ]]; then
	echo "Mixing reads to simulate datasets..."
	for readtype in "staggered" "unstaggered"
	do
		for err_profile in "q10" "q20" "q30" "q40"
		do
			for wtprop in $(seq 0 10 100) 2 5 95 98
			do
				echo "Simulating $readtype reads for $err_profile with $wtprop percent wildtype..."
				## for each error profile and wt proportion, make new "sample file" 
				outdir="final_datasets/${err_profile}"
				mkdir -p $outdir
				outprefix="${outdir}/wt${wtprop}${readtype}"
				## empty previous output file (outfile) if they existed
				echo -n > "${outprefix}_R1_spy101.fastq"
				echo -n > "${outprefix}_R2_spy101.fastq"

				## for each of three possible inputs, calculate the number of reads needed
				for edit in "wt" "1bpins" "15bpdel"
				do
					fastqprefix="spy101_amplicon_rev_${err_profile}_${edit}"

					if [[ $edit == "wt" ]]; then
						numreads=$(bc <<< "${totalreads}*${wtprop}/100")
					else
						indelprop=$(bc <<< "scale=2; (100-${wtprop})/2" )  # half of edit prop is for each edit
						numreads=$(bc <<< "${totalreads}*${indelprop}/100")
					fi
					## pull out $numreads from read1 and read2 files
					curr_run_random=$RANDOM ## need same for seed read1 and read 2
					for readside in 1 2
					do
						indir="final_${readtype}_fastq/${err_profile}"
						inbasefile="${edit}${readtype}_spy101_amp2_rev_R${readside}_${err_profile}.fastq.gz"
						infile="${indir}/${inbasefile}"
						## get the number of reads you are choosing from
						linesincurrfile=`zcat ${infile} | wc -l | cut -f1 -d" "`
						readsincurrfile=$(( $linesincurrfile / 4 ))

						outfile="${outprefix}_R${readside}_spy101.fastq"
						zcat ${infile} | awk -vseed=$curr_run_random -vreadstogenerate=$numreads -vreadsincurrfile=$readsincurrfile -f mix_datasets.awk >> $outfile
					done
				done
				## empty previous output file (outfile) if they existed
				gzip -f "${outprefix}_R1_spy101.fastq"
				gzip -f "${outprefix}_R2_spy101.fastq"
			done
		done
	done
fi

## run ./run_simreads.sh and wait for jobs to finish
./run_simreads.sh

## aggregate observed results
results_file="aggregate_results.txt"
if [[ 1 -eq 1 ]]; then
	first_file=0
	for qscore in q10 q20 q30 q40 
	do
		echo "Aggregating $qscore results..."
		for prop in 0 2 5 $(seq 10 10 90) 95 98 100
		do
			for readtype in "staggered" "unstaggered"
			do
				sampleid="wt${prop}${readtype}"
				resultsdir="results/${qscore}/deliverables/${sampleid}"
				## see what happened to reads throughout pipeline (reads merged and dropped)
				readsmerged_file="${resultsdir}/${sampleid}.reads_merged.txt"
				readsdropped_file="${resultsdir}/${sampleid}.reads_dropped.txt"
				## aggregate %editing and %unedited 
				propresults_file="${resultsdir}/${sampleid}.summary_result.txt"
				## also aggregate %1bp ins and %15 bp del
				indelsummary_file="${resultsdir}/${sampleid}.indel_profile.txt"
				## if file exists and non-zero size, include it
				if [[ -s $indelsummary_file ]]; then
					echo "Aggregating for $qscore with ${prop}% wt with $readtype reads..."
					if [[ $first_file -eq 0 ]]; then
						## overwrite existing file (if exists); get the column names from the wt100.txt file
						awk -v OFS="\t" '(NR==1){print "quality", "wtprop", "readtype", $0, "1bp_ins_reads", "1bp_ins_prop", "1bp_ins_score", "15bp_del_reads", "15bp_del_prop", "15bp_del_score"}' $propresults_file > $results_file
						first_file=1
					fi

					awk -v OFS="\t" -v qscore=$qscore -v prop=$prop	-v propresults_file=$propresults_file -v readtype=${readtype} '
						BEGIN{ 
							while(getline < propresults_file) {
								if (NR!=1){
									save[qscore, prop]=$0
								}
							}
							del_15bp_reads=0
							del_15bp_prop=0
							del_15bp_scores=NA
							ins_1bp_reads=0
							ins_1bp_prop=0
							ins_1bp_scores=NA
						}
						($7=="AAGCTA---------------ACAGGCTCCAGGAAGGGTT"){
							del_15bp_reads=$3
							del_15bp_prop=$4;
							del_15bp_score=$5;
						}
						($7=="AAGCTAACAGTTGCTTTTATCACAGGCTCCAGGAAGGGTT;20_T"){
							ins_1bp_reads=$3;
							ins_1bp_prop=$4;
							ins_1bp_score=$5;
						}
						END{ 
							print qscore, prop, readtype, save[qscore, prop], ins_1bp_reads, ins_1bp_prop, ins_1bp_score, del_15bp_reads, del_15bp_prop, del_15bp_score
						}
					' $indelsummary_file >> $results_file
				fi
			done
		done
	done
fi

## generate spreadsheet of expected vs observed results
exp_obs_file="exp_obs.txt"
exp_obs_diff_file="exp_obs_diff.txt"
if [[ 1 -eq 1 ]]; then
	echo "Formatting expected vs observed results..."
	awk -vFS="\t" -vOFS="\t" -v exp_numreads=$totalreads '
	BEGIN{ print "qscore", "wt_perc", "readtype", "exp_numreads", "exp_ref_prop", "exp_mismatch_prop", "exp_unedited_prop", "exp_1bp_ins_prop", "exp_15bp_del_prop", "exp_other_prop", "exp_edited_prop", "obs_numreads", "obs_ref_prop", "obs_mismatch_prop", "obs_unedited_prop", "obs_1bp_ins_prop", "obs_15bp_del_prop", "obs_other_prop", "obs_edited_prop" } 
	(NR!=1){print $1, $2, $3, exp_numreads, $2/100, 0, $2/100, (100-$2)/2/100, (100-$2)/2/100, 0, (100-$2)/100,  $10, $11, $13, $11+$13, $17, $20, $12-$17-$20, $12}
	' $results_file > $exp_obs_file
	awk -vFS="\t" -vOFS="\t" '
	(NR==1){print $0, "diff_numreads", "diff_ref_prop", "diff_mismatch_prop", "diff_unedited_prop", "diff_1bp_ins_prop", "diff_15bp_del_prop", "diff_other_prop", "diff_edited_prop"}
	(NR!=1){print $0, $12-$4, $13-$5, $14-$6, $15-7, $16-$8, $17-$9, $18-$10, $19-$11}' $exp_obs_file > $exp_obs_diff_file
fi

## plot visualizations of expected and observed
if [[ 1 -eq 1 ]]; then
	outfile="plots.pdf"
	echo "Plotting visualizatin of expected vs observed results in ${outfile}..."
	Rscript plot_exp_obs.R $exp_obs_diff_file $outfile
fi

