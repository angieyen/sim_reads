($7=="AAGCTA---------------ACAGGCTCCAGGAAGGGTT"){
	del_15bp_reads=$3
	del_15bp_prop=$4;
	del_15bp_score=$5;
}
($7=="AAGCTAACAGTTGCTTTTATCACAGGCTCCAGGAAGGGTT;20_T"){
	ins_1bp_reads=$3;
	ins_1bp_prop=$4;
	ins_1bp_score=$5;
}
END{ 
	print qscore, prop, ins_1bp_reads, ins_1bp_prop, ins_1bp_score, del_15bp_reads, del_15bp_prop, del_15bp_score
}
