#!/bin/bash
## make error profile for dataset where all bases and positions are the same quality score; this is passed in as input (between 2 and 38)
## based on description in README file: /cluster/home/yena/software/art_bin_MountRainier/art_illumina_README.txt 
quality_score=$1

readlength=300
min_score=2
max_score=40
skip="3 4 5 6"

## assume uniform quality scores 
for base in "." "A" "T" "G" "C"
do
	for position in $(seq 0 $readlength)
	do
		for round in 1 2
		do
			printf "%s\t%s\t" "$base" "$position"
			#for score in $(seq $min_score $max_score)
			for score in $quality_score
			do
				if (echo $skip | fgrep -q -w "$score"); then
					continue
				else
					if [[ $round == 1 ]]; then
						printf "%s\t" "$score"
					elif [[ $round == 2 ]]; then
						if [[ "$score" -ge "$quality_score" ]]; then
							## 4 nucleotides, so "." (which includes all 4) has count of 400
							if [[ $base == "." ]]; then
								printf "400\t"
							## "count" of 100 for each nucleotide
							else
								printf "100\t"
							fi
						else
							printf "0\t"
						fi
					fi
				fi
			done
			printf "\n"
		done
	done
done

